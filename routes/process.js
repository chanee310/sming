const express = require('express');
const router = express.Router();
const fs = require('fs');

const saveFileFromBase64 = (filename, binaryData) => {
    return new Promise((resolve) => {
        fs.writeFile(filename, binaryData, 'base64', (err) => {
            // console.log(`[*] 사진 저장됨. filename : ${filename}`);
            resolve();
        });
    })
}

router.post('/', (req, res, next) => {
    let data = req.body.image_data.replace(/^data:image\/(png|jp(e|)g);base64,/, '');
    let filename = `./tmp/sming_${Date.now()}.png`;
    saveFileFromBase64(filename, data).then(() => {
        res.json({
            result: 1,
            filename: filename.split('/').pop(),
        });
    });
});

module.exports = router;