const
    express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'), 
    schedule = require('node-schedule'),
    rmdir = require('rmdir'),
    fs = require('fs');

const
    process = require('./routes/process');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '20mb'
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/save', process);

// catch 404 and forward to error handler
// app.use((req, res, next) => {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// // error handlers

// // development error handler
// // will print stacktrace
// if (app.get('env') === 'development') {
//     app.use((err, req, res, next) => {
//         res.status(err.status || 500);
//         res.render('error', {
//             message: err.message,
//             error: err
//         });
//     });
// }

// production error handler
// no stacktraces leaked to user
// app.use((err, req, res, next) => {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });

// 파일 삭제 데몬
// schedule.scheduleJob('30 0 1 * * *', () => {
//     rmdir(path.join(__dirname, 'tmp'), (err, dirs, files) => {
//         fs.mkdirSync(path.join(__dirname, 'tmp'));
//         console.log('[*] tmp 폴더 비움');
//     });
// });

module.exports = app;
